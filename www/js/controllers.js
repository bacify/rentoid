angular.module('app.controllers', ['ionic', 'ngCordova'])


.run(function($rootScope,$http) {
    
    //inject global variable
  $rootScope.apiURL="http://127.0.0.1/apiserver/";
 // $rootScope.apiURL='http://rentoid.bugsto.com/';
    $rootScope.imgURL="http://localhost/tugas/assets/uploads/files/store";
    $rootScope.api_key=0;
    var url=$rootScope.apiURL+"/login/guest";;
    console.log(localStorage.getItem("logged"));
    if(localStorage.getItem("logged") !== null && localStorage.getItem("logged") !== "" &&localStorage.getItem("logged") !== "undefined"){
       $rootScope.api_key=localStorage.getItem("api_key");
     //  $httpProvider.defaults.headers.get = { 'X-API-KEY' : $rootScope.api_key };
       console.log(localStorage.getItem("api_key"));
       
    }else {
        
      $http.get(url).then(function(response){
        //  console.log(JSON.stringify(response.data));  
         localStorage.setItem("logged", false);
        localStorage.setItem("api_key", response.data.key);
        $rootScope.api_key=response.data.key;
        console.log(localStorage.getItem("api_key"));
        console.log(response.data);
        });
    }

    
    var status=localStorage.getItem("logged");
    if(status=='true')
     $rootScope.login_status=true;
   else
    $rootScope.login_status=false;
    console.log($rootScope.login_status);

     var cart=localStorage.getItem("cart");
     if(cart!='null'&& cart!='undefined')
      $rootScope.cart=cart;
    $rootScope.tempstate=2;
       
})
  
.controller('homeCtrl', ['$scope', '$stateParams','$ionicFilterBar','$http',
 // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$ionicFilterBar,$http,$rootScope) {

  
   /*  var filterBarInstance;

      function getItems () {
        var items = [];
        for (var x = 1; x < 2000; x++) {
          items.push({text: 'This is item number ' + x + ' which is an ' + (x % 2 === 0 ? 'EVEN' : 'ODD') + ' number.'});
        }
        $scope.items = items;
      }

      getItems();

      $scope.showFilterBar = function () {

        filterBarInstance = $ionicFilterBar.show({
          items: $scope.items,
          update: function (filteredItems, filterText) {
            $scope.items = filteredItems;
            if (filterText) {
              console.log(filterText);
            }
          }
        });
      };

      $scope.refreshItems = function () {
        if (filterBarInstance) {
          filterBarInstance();
          filterBarInstance = null;
        }

        $timeout(function () {
          getItems();
          $scope.$broadcast('scroll.refreshComplete');
        }, 1000);
      };
   */  
    
var url=$scope.apiURL+"/kategori";
  $scope.kategori=[];
    $http.get(url).then(function(response){
        //  console.log(JSON.stringify(response.data));  
        $scope.kategori=response.data;
console.log(response);
        });
    console.log("tolet");

}])
.controller('home-viewCtrl', ['$scope', '$stateParams','$http','$ionicHistory','$rootScope','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$ionicHistory,$rootScope,$state) {

    console.log($scope.login_status);
    console.log('home '+$scope.api_key);

    var url=$scope.apiURL+"/api/product";
    $scope.categories=[];
    $http({
    method:'GET',
    url:url,
    headers: {
      'X-API-KEY' : $scope.api_key
    }
    }).then(function(response){
        //  console.log(JSON.stringify(response.data));  
        $scope.categories=response.data.product;
         console.log(response.data);
      


        }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
           
            console.log(response.data);
             console.log('abc');
            $ionicHistory.nextViewOptions({
             disableBack: true
            });
            //$state.go('rentoid.homeapp');
            $rootScope.tempstate=$scope.tempstate+1;
            $state.go('rentoid.homeapp', {state:$scope.tempstate}, {reload: true});
        });



}]) 
.controller('feed-viewCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}]) 
.controller('fav-viewCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])   
.controller('cartCtrl', ['$scope', '$stateParams','$http','$ionicScrollDelegate','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$ionicScrollDelegate,$state) {


  var cart=localStorage.getItem("cart");
  $scope.cart_status;
  $scope.cart_product;
  $scope.cart=cart;
  $scope.totalprice=0;
  $scope.notification='';
  if(cart){
    getcart();
  }
  $scope.toko=[];
  function getcart(cart){
   var url=$scope.apiURL+"/book/cart";
   console.log(url);
    $scope.categories=[];
    $http({
    method:'POST',
    url:url,
    data:{
         
          cart_id:$scope.cart
          
        },
    headers: {
      'X-API-KEY' : $scope.api_key
    }
    }).then(function(response){
        //  console.log(JSON.stringify(response.data));  
          $scope.cart_product=response.data.product;

          $scope.cart_status=response.data.cart;
          
          for(var x=0;x<response.data.product.length;x++){
            var var_store=false;
              for(var y=0;y<$scope.toko.length;y++){
                if($scope.toko[y].store_id!=response.data.product[x].store_id){
                  var_store=true;
                }
              }
              if($scope.toko.length==0){
                var_store=true;
              }
              if(var_store){
               $scope.toko.push({store_id:response.data.product[x].store_id,store_name:response.data.product[x].store_name}); 
              }
              //conver qty to int
              $scope.cart_product[x].qty=parseInt($scope.cart_product[x].qty);
             $scope.totalprice= $scope.totalprice+($scope.cart_product[x].price*$scope.cart_product[x].qty);
          }
          //console.log(toko);
          

          console.log(response.data.product);
          console.log(response.data.cart);
        });
  }

  $scope.removeproduct=function(pid){
    var product_name='';
    var cart=0;
    for(var a=0;a<$scope.cart_product.length;a++){
      
        if($scope.cart_product[a].product_id==pid){
          product_name=$scope.cart_product[a].name;
          cart=$scope.cart_product[a].cart_id;


         
        }
        console.log(parseInt($scope.cart_product[a].product_id)+' '+parseInt(a));
       
      }
      
    var status=confirm( 'delete product '+product_name+'?');

    console.log(status);
    console.log(cart);
    if(status){
      console.log('proses hapus product');
       removecart(cart,pid);
    }
  }

  function removecart(cartid,product_id){

    var url=$scope.apiURL+"/book/deletefromcart/cart_id/"+cartid+"/product_id/"+product_id;
    console.log(url);
      $http({
      method:'GET',
      url:url,
      headers: {
        'X-API-KEY' : $scope.api_key
      }
      }).then(function(response){
          //  console.log(JSON.stringify(response.data));  
           
            $scope.notification=response.data.message;
            getcart()
            //$scope.cart_status=response.data.cart;
            
            $ionicScrollDelegate.scrollTop();
            //console.log(toko);
            console.log(response.data);
            //console.log(response.data.cart);
          });
  }

  $scope.updatecart=function(){
    //  console.log(JSON.stringify($scope.cart_product));

      var url=$scope.apiURL+"/book/cartupdate";

      for(var x=0;x<$scope.cart_product.length;x++){
        console.log($scope.cart_product[x].product_id+' '+$scope.cart_product[x].qty);
        var produk_id=$scope.cart_product[x].product_id;
        var produk_qty=$scope.cart_product[x].qty;
        var produk_note=$scope.cart_product[x].note;
           $http({
            method:'POST',
            url:url,
            data:{
                 
                  cart_id:$scope.cart,
                  product_id:produk_id,
                  qty:produk_qty,
                  note:produk_note
                  
                },
            headers: {
              'X-API-KEY' : $scope.api_key
            }
            }).then(function(response){
                  console.log(response.data);  
                 
                });

      }
  }

 $scope.booking=function(store_id){
       console.log('proses booking'+store_id);
      $state.go('rentoid.bookingform', {store_id:store_id}, {reload: true});
 }
 
  $scope.getTotalPrice = function (store_id) {
        var total=0;
       // console.log($scope.cart_product.length);
        for(var x=0;x<$scope.cart_product.length;x++){

        
          if($scope.cart_product[x].store_id==store_id){

             total= total+($scope.cart_product[x].price*$scope.cart_product[x].qty);
             }
          }

          return total;
      };
  

}])
   
.controller('bookingformCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http) {

$scope.form={'tgl_start':new Date(),
             'tgl_end':new Date(),
            'telp':''};

 $scope.user=localStorage.getItem("user_id");
 $scope.store_id=$stateParams.store_id;

  var cart=localStorage.getItem("cart");
  $scope.cart_status;
  $scope.cart_product;
  $scope.cart=cart;
  $scope.totalprice=0;
  $scope.notification='';
  if(cart){
    getcart();
  }
  $scope.toko=[];
  function getcart(cart){
   var url=$scope.apiURL+"/book/cart";
   console.log(url);
    $scope.categories=[];
    $http({
    method:'POST',
    url:url,
    data:{
         
          cart_id:$scope.cart
          
        },
    headers: {
      'X-API-KEY' : $scope.api_key
    }
    }).then(function(response){
        //  console.log(JSON.stringify(response.data));  
          $scope.cart_product=response.data.product;

          $scope.cart_status=response.data.cart;
          
          for(var x=0;x<response.data.product.length;x++){
            var var_store=false;
              for(var y=0;y<$scope.toko.length;y++){
                if($scope.toko[y].store_id!=response.data.product[x].store_id){
                  var_store=true;
                }
              }
              if($scope.toko.length==0){
                var_store=true;
              }
              if(var_store){
               $scope.toko.push({store_id:response.data.product[x].store_id,store_name:response.data.product[x].store_name}); 
              }
              //conver qty to int
              $scope.cart_product[x].qty=parseInt($scope.cart_product[x].qty);
             $scope.totalprice= $scope.totalprice+($scope.cart_product[x].price*$scope.cart_product[x].qty);
          }
          //console.log(toko);
          

          console.log(response.data.product);
          console.log(response.data.cart);
        });
  }

  $scope.getTotalPrice = function (store_id) {
        var total=0;
       // console.log($scope.cart_product.length);
        for(var x=0;x<$scope.cart_product.length;x++){

        
          if($scope.cart_product[x].store_id==store_id){

             total= total+($scope.cart_product[x].price*$scope.cart_product[x].qty);
             }
          }

          return total;
      };

 $scope.checkout=function(){
   

        var url=$scope.apiURL+"/book/cartcheckout";
       console.log($scope.form.tgl_start.getTime()/1000+' end'+$scope.form.tgl_end.getTime()/1000+' telp'+$scope.form.telp);
      
        
           $http({
            method:'POST',
            url:url,
            data:{
                 
                  cart_id:cart,
                  user_id:$scope.user,
                  start:parseInt(($scope.form.tgl_start.getTime() / 1000).toFixed(0)),
                  finish:parseInt(($scope.form.tgl_end.getTime() / 1000).toFixed(0)),
                  no_telp:$scope.form.telp,
                  store_id:$scope.store_id

                },
            headers: {
              'X-API-KEY' : $scope.api_key
            }
            }).then(function(response){
                  console.log(response.data);  
                 
                });

      }

}])  
.controller('cloudCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('rentoidCtrl', ['$scope', '$stateParams','$http','$state','$ionicHistory','$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$state,$ionicHistory,$rootScope) {
 
  $scope.mytoko = function(){
     $ionicHistory.nextViewOptions({
         disableBack: true
      });
   $state.go('rentoid.mytoko', {state:$scope.tempstate}, {reload: true});

  }

 $scope.logout = function(){


        localStorage.clear();
        console.log('logout');

        var url=$scope.apiURL+"/login/guest";;
         $http.get(url).then(function(response){
          //  console.log(JSON.stringify(response.data));  
          localStorage.setItem("logged", false);
          localStorage.setItem("api_key", response.data.key);
          $rootScope.login_status=false;
          console.log(response.data);
        });
      }

}])
   
.controller('productCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
.controller('bookingCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http) {

    $scope.list_booking=[];
    
    initdata();

    function initdata(){
       
       var user=localStorage.getItem("user_id");
       var url=$scope.apiURL+"/book/booklist/user_id/"+user;
   
       
        $http({
        method:'GET',
        url:url,
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
     
            $scope.list_booking=response.data.book_list;
            console.log(response.data);
            
            });
    }

}])
.controller('loginCtrl', ['$scope', '$stateParams','$rootScope','$http','$state','$ionicHistory','$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$rootScope,$http,$state,$ionicHistory,$rootScope) {
 function initdata(){
    
      $scope.formdata={ name: "", email: "",password: ""};
      
      
      
      if($scope.login_status==true){
        console.log('abc');
        $ionicHistory.nextViewOptions({
         disableBack: true
        });
        //$state.go('rentoid.homeapp');
        $rootScope.tempstate=$scope.tempstate+1;
        $state.go('rentoid.homeapp', {state:$scope.tempstate}, {reload: true});
      }
      
  }
  initdata();
  $scope.login = function(){
     
      $scope.notification=null;
      var url=$scope.apiURL+"/login/auth";
        
        $http({
        method:'POST',
        url:url,
        data:{
         
          email:$scope.formdata['email'],
          password:$scope.formdata['password']
        },
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
            

            
            if(response.data.status){
              var key=response.data.key;
              var id=response.data.user_id;
              var status=response.data.status;
              var store_id=response.data.store_id;
              localStorage.setItem("logged", status);
              localStorage.setItem("api_key", key);
              localStorage.setItem("user_id", id);
              localStorage.setItem("store_id", store_id);
              $rootScope.login_status=true;

              initdata();
            }
            $scope.notification=response.data.message;
          //  console.log(response.data.message);
          }, function errorCallback(response) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
            $scope.notification=response.data.message;
            console.log(response.data.message);
        });
    }



}])

.controller('registerCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http) {
 function initdata(){
    
      $scope.formdata={ name: "", email: "",password: ""};
      
     
  }
  initdata();

    $scope.register = function(){
     
      $scope.notification=null;
      var url=$scope.apiURL+"/login/register";
        
        $http({
        method:'POST',
        url:url,
        data:{
          nama: $scope.formdata['name'],
          email:$scope.formdata['email'],
          password:$scope.formdata['password']
        },
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
            $scope.response=response.data;
            $scope.notification=response.data.message;
            var status=response.data.status;
            console.log(status);
            console.log(response.data);
                  if(status==0){
                        
                   
                  }else{
                    initdata();
                  }
            
            });
    }

}])
.controller('daftar_tokoCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
.controller('editprofil_tokoCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http) {

   function initdata(){
    
      $scope.formdata={ nama: "", alamat:""};
       var user=localStorage.getItem("user_id");
       var url=$scope.apiURL+"/api/store/user_id/"+user;
   
       
        $http({
        method:'GET',
        url:url,
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
     
            $scope.formdata['nama']=response.data.name;
          $scope.formdata['alamat']=response.data.address;
          //  console.log(response.data);
            
            });
    
     
  }
  initdata();

  $scope.updatedata=function(){

      var store_id=localStorage.getItem("store_id");
      var url=$scope.apiURL+"/api/store";
   
       
        $http({
        method:'POST',
        url:url,
        data:{
            store_id:store_id,
            nama:$scope.formdata['nama'],
            alamat:$scope.formdata['alamat'],
            action:'update'
        },
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
     
          
           console.log(response.data);
            initdata();
            });

  }


}])
.controller('editprofil_userCtrl', ['$scope', '$stateParams','$cordovaActionSheet','$cordovaCamera', '$cordovaFile', '$cordovaFileTransfer', '$cordovaDevice', '$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $cordovaActionSheet,$ionicActionSheet,$timeout) {
  $scope.image = null;
 
 $scope.show = function() {

   // Show the action sheet
   var hideSheet = $ionicActionSheet.show({
     buttons: [
       { text: '<b>Share</b> This' },
       { text: 'Move' }
     ],
     destructiveText: 'Delete',
     titleText: 'Modify your album',
     cancelText: 'Cancel',
     cancel: function() {
          // add cancel code..
        },
     buttonClicked: function(index) {
       return true;
     }
   });
 
   // For example's sake, hide the sheet after two seconds
   $timeout(function() {
     hideSheet();
   }, 2000);

 };

  $scope.showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  // Present Actionsheet for switch beteen Camera / Library
$scope.loadImage = function() {
  var options = {
    title: 'Select Image Source',
    buttonLabels: ['Load from Library', 'Use Camera'],
    addCancelButtonWithLabel: 'Cancel',
    androidEnableCancelButton : true,
  };
  $cordovaActionSheet.show(options).then(function(btnIndex) {
    var type = null;
    if (btnIndex === 1) {
      type = Camera.PictureSourceType.PHOTOLIBRARY;
    } else if (btnIndex === 2) {
      type = Camera.PictureSourceType.CAMERA;
    }
    if (type !== null) {
      $scope.selectPicture(type);
    }
  });
};


// Take image with the camera or from library and store it inside the app folder
// Image will not be saved to users Library.
$scope.selectPicture = function(sourceType) {
  var options = {
    quality: 100,
    destinationType: Camera.DestinationType.FILE_URI,
    sourceType: sourceType,
    saveToPhotoAlbum: false
  };
 
  $cordovaCamera.getPicture(options).then(function(imagePath) {
    // Grab the file name of the photo in the temporary directory
    var currentName = imagePath.replace(/^.*[\\\/]/, '');
 
    //Create a new name for the photo
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
 
    // If you are trying to load image from the gallery on Android we need special treatment!
    if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
      window.FilePath.resolveNativePath(imagePath, function(entry) {
        window.resolveLocalFileSystemURL(entry, success, fail);
        function fail(e) {
          console.error('Error: ', e);
        }
 
        function success(fileEntry) {
          var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
          // Only copy because of access rights
          $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
            $scope.image = newFileName;
          }, function(error){
            $scope.showAlert('Error', error.exception);
          });
        };
      }
    );
    } else {
      var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      // Move the file to permanent storage
      $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
        $scope.image = newFileName;
      }, function(error){
        $scope.showAlert('Error', error.exception);
      });
    }
  },
  function(err){
    // Not always an error, maybe cancel was pressed...
  })
};



// Returns the local path inside the app for an image
$scope.pathForImage = function(image) {
  if (image === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + image;
  }
};



}])
.controller('List_produkCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http) {

$scope.products=null;
var url=$scope.apiURL+"/api/product";;
  
     $http({
    method:'GET',
    url:url,
    headers: {
      'X-API-KEY' : $scope.api_key
    }
    }).then(function(response){
        //  console.log(JSON.stringify(response.data));  
        $scope.products=response.data.product;
    console.log(JSON.stringify($scope.products));
        });


}])
.controller('List_orderCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http) {
   $scope.list_booking=[];
    
    initdata();

    function initdata(){
       
       var user=localStorage.getItem("store_id");
       var url=$scope.apiURL+"/book/storebooklist/store_id/"+user;
   
       
        $http({
        method:'GET',
        url:url,
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
     
            $scope.list_booking=response.data.book_list;
            console.log(response.data);
            
            });
    }

}])
.controller('produk_detailCtrl', ['$scope', '$stateParams','$http','$ionicScrollDelegate','$rootScope','$state','$ionicHistory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http,$ionicScrollDelegate,$rootScope,$state,$ionicHistory) {
 var cart=null; 
 cart=localStorage.getItem("cart");
  
 $scope.store_id=localStorage.getItem('store_id');

  function initdata(){
    $scope.products={nama:'no data',
                    image:'assets/image/products/SL.png',
                      deskripsi:'no data',
                      harga:'0',
                      store_id:''
                    };
            
          if(cart!='undefined'){
            $scope.cart=cart;
            
          }
           $scope.notification=$stateParams.msg;
  }

  initdata();
  var id = $stateParams.id; 
  if(id==null){
    id='3';
  }
  $scope.products='';
  var url=$scope.apiURL+"api/product/id/"+id;

  console.log(url);
    $http({
    method:'GET',
    url:url,
    headers: {
      'X-API-KEY' : $scope.api_key
    }
    }).then(function(response){
        //  console.log(JSON.stringify(response.data));  
        
        if(response.data.product.product_id){
            $scope.products=response.data.product;
        }else{
            initdata();
           
        }
        //  console.log(response.data);
          console.log(response.data.product);
        });

  $scope.addtocart=function(){
      var a=$scope.products.product_id;
      var url=$scope.apiURL+"book/addtocart/product_id/"+a;
         if($scope.cart!=null && $scope.cart!='undefined'){
            console.log($scope.cart);
            url=url+"/cart_id/"+$scope.cart;
         }
        console.log(url);
        $http({
        method:'GET',
        url:url,
        headers: {
          'X-API-KEY' : $scope.api_key
        }
        }).then(function(response){
            //  console.log(JSON.stringify(response.data));  
            
            if(response.data.message)
            {    
              $scope.notification=response.data.message;
              if($scope.cart=='undefined'||$scope.cart==null){
                 localStorage.setItem("cart", response.data.cart_id);
                 $rootScope.cart=response.data.cart_id;
                 $scope.cart=response.data.cart_id;

              }
               $ionicHistory.nextViewOptions({
               disableBack: true
              });
              $state.go('rentoid.produk_detail', {id:id,msg:response.data.message}, {reload: true});

            }
           // $ionicScrollDelegate.scrollTop();
            //  console.log(response.data);
              console.log(response.data);
            });
        
        
        
    
   

  }



  


}])
.controller('mytokoCtrl', ['$scope', '$stateParams','$http','$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http,$rootScope) {
  
  $scope.namatoko=null;
  $scope.alamattoko="";
  $scope.telp="08965755755";
   console.log('state'+ $stateParams.state);
  initdata();
  function initdata(){
        $rootScope.tempstate=$scope.tempstate+1;

        var user_id=localStorage.getItem('user_id');
        var url=$scope.apiURL+"/api/store/user_id/"+user_id;
       console.log('init data');
           $http({
          method:'GET',
          url:url,
          headers: {
            'X-API-KEY' : $scope.api_key
          }
          }).then(function(response){
            $scope.namatoko=response.data.name;
            $scope.alamattoko=response.data.address;

              //  console.log(JSON.stringify(response.data));  
             //  $scope.products=response.data;
        //  console.log(JSON.stringify(response.data));
        });
  }
  $scope.products=null;
  var store_id=localStorage.getItem('store_id');
  var url=$scope.apiURL+"/api/product/store_id/"+store_id;
  console.log(url);
     $http({
    method:'GET',
    url:url,
    headers: {
      'X-API-KEY' : $scope.api_key
    }
    }).then(function(response){
        //  console.log(JSON.stringify(response.data));  
         $scope.products=response.data.product;
        console.log(response.data);
        });
  

}])
 .controller('addproductCtrl', ['$scope', '$stateParams','$http','$cordovaActionSheet','$cordovaCamera', '$cordovaFile', '$cordovaFileTransfer', '$cordovaDevice', '$ionicPopup','$timeout', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http,$cordovaActionSheet,$cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup,$timeout) {

$scope.image = null;
var imageurl='';
  console.log($cordovaActionSheet);


  $scope.formdata={nama:'',harga:'',keterangan:''};
  $scope.notification=null;
  $scope.formstatus=true;
initdata();
  function initdata(){
       $scope.formdata={nama:'',harga:'',keterangan:''};
  }
  $scope.insert_product=function(){
      var nama=$scope.formdata['nama'];
      var harga=$scope.formdata['harga'];
      var keterangan=$scope.formdata['keterangan'];
      var image=imageurl;
      if(nama!==''&&harga!==''&&keterangan!==''){
        if(isNaN(harga)){
          $scope.notification='cek form anda, form harga harus Angka ';
          console.log('harga salah');  
        }else{
          
          var data={
            store_id:localStorage.getItem('store_id'),
            nama:nama,
            harga:harga,
            keterangan:keterangan,
            image:image,
            action:'add'
          }
           
            var user_id=localStorage.getItem('store_id');
            var url=$scope.apiURL+"/api/product/";
           // console.log(url);
            var msg='';
              $http({
                method:'POST',
                url:url,
                data:data,
                headers: {
                  'X-API-KEY' : $scope.api_key
                }
              }).then(function(response){
              //  console.log(JSON.stringify(response.data));  
                  $scope.notification=response.data.message;
                  var status=response.data.status;
                  if(status===1){
                    $scope.formstatus=false;
                    initdata();
                    console.log(data);
                    console.log('data diproses');             
                  }else{
                    console.log('data tidak dapat diproses server');             
                  }
                 
              });
        
        

          
        }
      }else{
        $scope.notification='data tidak dapat diproses, pastikan form terisi dengan benar';
        console.log('data tidak dapat diproses');
        console.log($scope.formdata);
      }
  };
  
  $scope.showAlert = function(title, msg) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

$scope.loadImage = function() {
  var options = {
    title: 'Select Image Source',
    buttonLabels: ['Load from Library', 'Use Camera'],
    addCancelButtonWithLabel: 'Cancel',
    androidEnableCancelButton : true,
  };
  $cordovaActionSheet.show(options).then(function(btnIndex) {
    var type = null;
    if (btnIndex === 1) {
      type = Camera.PictureSourceType.PHOTOLIBRARY;
    } else if (btnIndex === 2) {
      type = Camera.PictureSourceType.CAMERA;
    }
    if (type !== null) {
      $scope.selectPicture(type);
    }
  });
};

  // Take image with the camera or from library and store it inside the app folder
// Image will not be saved to users Library.
$scope.selectPicture = function(sourceType) {
  var options = {
    quality: 80,
    destinationType: Camera.DestinationType.FILE_URI,
    sourceType: sourceType,
    saveToPhotoAlbum: false,
    correctOrientation:true
  };

  $cordovaCamera.getPicture(options).then(function(imagePath) {
    // Grab the file name of the photo in the temporary directory
    var currentName = imagePath.replace(/^.*[\\\/]/, '');

    //Create a new name for the photo
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";

    // If you are trying to load image from the gallery on Android we need special treatment!
    if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
      window.FilePath.resolveNativePath(imagePath, function(entry) {
        window.resolveLocalFileSystemURL(entry, success, fail);
        function fail(e) {
          console.error('Error: ', e);
        }

        function success(fileEntry) {
          var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
          // Only copy because of access rights
          $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
            $scope.image = newFileName;
          }, function(error){
            $scope.showAlert('Error', error.exception);
          });
        };
      }
      );
    } else {
      var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      // Move the file to permanent storage
      $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
        $scope.image = newFileName;
      }, function(error){
        $scope.showAlert('Error', error.exception);
      });
    }
  },
  function(err){
    // Not always an error, maybe cancel was pressed...
  })
  };



// Returns the local path inside the app for an image
$scope.pathForImage = function(image) {
  if (image === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + image;
  }
};


$scope.uploadImage = function() {
  // Destination URL
 var url=$scope.apiURL+"/api/image/";
 console.log('proses '+url);
  // File for Upload
  var targetPath = $scope.pathForImage($scope.image);
 
  // File name only
  var filename = $scope.image;;
  imageurl='assets/image/products/'+filename;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename,
              'X-API-KEY' : $scope.api_key}
  };
  
 console.log($cordovaFileTransfer);
  $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
    console.log(result);
    

    $scope.insert_product();
  }, function(err) {
       console.log(err);    // Error
      }, function (progress) {
        $timeout(function () {
          $scope.downloadProgress = (progress.loaded / progress.total) * 100;
          $scope.notification='Uploading image '+Number($scope.downloadProgress).toFixed(2)+'%';
        });
      });
}

}])  
.controller('pageCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('searchCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

  $scope.showfilter= function(){
    $(".search-filter").toggle();
  };
}])
 
 .controller('resultCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http) {

var id=$stateParams.kategori_id;
console.log(id);
var url=$scope.apiURL+"/produk?cat="+id+"";
console.log(url);
  $scope.results=[];
  var config ={
    headers : {'key':'123456'}
  };
    $http.post(url, {},config ).then(function(response){
        console.log(JSON.stringify(response));  
          $scope.results=response.data;
  
            
        });

  $scope.showfilter= function(){
    $(".search-filter").toggle();
  };
}])
 