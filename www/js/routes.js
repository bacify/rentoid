angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  .state('rentoid', {
    url: '/side-menu21',
    templateUrl: 'templates/rentoid.html',
    controller: 'rentoidCtrl'
  })


      .state('rentoid.home', {
    url: '/home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
       
      }
    }
  })

      .state('rentoid.homeapp', {
    url: '/homeapp',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home-view.html',
        controller: 'home-viewCtrl'
      }
    },

    params: {
    state: null
    }
  })
         .state('rentoid.home.feedapp', {
    url: '/feedapp',
    views: {
      'feedview': {
        templateUrl: 'templates/feed-view.html',
        controller: 'feed-viewCtrl'
      }
    }
  })
              .state('rentoid.home.favapp', {
    url: '/favapp',
    views: {
      'favview': {
        templateUrl: 'templates/fav-view.html',
        controller: 'fav-viewCtrl'
      }
    }
  })
  .state('rentoid.cart', {
    url: '/cart',
    views: {
      'side-menu21': {
        templateUrl: 'templates/cart.html',
        controller: 'cartCtrl'
      }
    },

    params: {
    state: null
    }
  })
.state('rentoid.login', {
    url: '/login',
    views: {
      'side-menu21': {
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      }
    }
  })
.state('rentoid.register', {
    url: '/register',
    views: {
      'side-menu21': {
        templateUrl: 'templates/register.html',
        controller: 'registerCtrl'
      }
    }
  })
.state('rentoid.daftar_toko', {
    url: '/daftar_toko',
    views: {
      'side-menu21': {
        templateUrl: 'templates/daftar_toko.html',
        controller: 'daftar_tokoCtrl'
      }
    }
  })
.state('rentoid.editprofil_toko', {
    url: '/editprofil_toko',
    views: {
      'side-menu21': {
        templateUrl: 'templates/editprofil_toko.html',
        controller: 'editprofil_tokoCtrl'
      }
    }
  })
.state('rentoid.editprofil_user', {
    url: '/editprofil_user',
    views: {
      'side-menu21': {
        templateUrl: 'templates/editprofil_user.html',
        controller: 'editprofil_userCtrl'
      }
    }
  })
.state('rentoid.List_produk', {
    url: '/List_produk',
    views: {
      'side-menu21': {
        templateUrl: 'templates/List_produk.html',
        controller: 'List_produkCtrl'
      }
    }
  })
.state('rentoid.List_order', {
    url: '/List_order',
    views: {
      'side-menu21': {
        templateUrl: 'templates/list_order.html',
        controller: 'List_orderCtrl'
      }
    }
  })
.state('rentoid.produk_detail', {
    url: '/produk_detail',
    views: {
      'side-menu21': {
        templateUrl: 'templates/produk_detail.html',
        controller: 'produk_detailCtrl'
      }
    },
    params: {
    id: null,
    msg: null
     }
  })
 .state('rentoid.mytoko', {
    url: '/mytoko',
     views: {
      'side-menu21': {
        templateUrl: 'templates/mytoko.html',
        controller: 'mytokoCtrl'
      }
    },
    params: {
    state: null
    }
  

  }) 
 .state('rentoid.addproduct', {
    url: '/addproduct',
     views: {
      'side-menu21': {
        templateUrl: 'templates/addproduct.html',
        controller: 'addproductCtrl'
      }
    }

  })

 .state('rentoid.search', {
    url: '/search',
     views: {
      'side-menu21': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
        }
    }
  })
 .state('rentoid.result', {
    url: '/searchresult',
     views: {
      'side-menu21': {
        templateUrl: 'templates/result.html',
        controller: 'resultCtrl'
        }
    },
    params: {
    kategori_id: null
    }
  })

  .state('rentoid.product', {
    url: '/product',
    templateUrl: 'templates/product.html',
    controller: 'productCtrl'
  })
  
  

 .state('rentoid.booking', {
    url: '/booking',
    views: {
      'side-menu21': {
        templateUrl: 'templates/booking.html',
        controller: 'bookingCtrl'
      }
    }
  })
  

.state('rentoid.bookingform', {
    url: '/bookingform',
    views: {
      'side-menu21': {
        templateUrl: 'templates/bookingform.html',
        controller: 'bookingformCtrl'
      }
    },
    params:{
      store_id:null
    }
  })

  .state('page', {
    url: '/page5',
    templateUrl: 'templates/page.html',
    controller: 'pageCtrl'
  })

 

$urlRouterProvider.otherwise('/side-menu21/homeapp')

  

});