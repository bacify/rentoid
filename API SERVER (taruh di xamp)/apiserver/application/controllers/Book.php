<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Book extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        
        $this->load->model('Booking_model');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
       /* $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key*/
    }


    /*category API call*/

    public function cart_post(){
        $cart=$this->post('cart_id');
        if($cart==null){
            $cart=$this->checkcart($header);
        }
        if($cart){

            $status_cart=$this->Booking_model->get_cart_status_id($cart);
            $detail_cart=$this->Booking_model->get_product_cart($cart);
          // print_r( $detail_cart);
             $this->response([
                    'status' => TRUE,
                    'message' => 'cart found',
                    'cart'=>$status_cart,
                    'product'=>$detail_cart
                 ], REST_Controller::HTTP_OK); // OK (200) being the 
        }else{
               $this->response([
                    'status' => FALSE,
                    'message' => 'no cart found'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function addtocart_get()
    {
        $product_id=$this->get('product_id');
        $header=$this->input->get_request_header('X-API-KEY', TRUE);
        $cart=$this->get('cart_id');
        $user_id=$this->Booking_model->get_user_id_by_key($header);
       // echo $user_id[0]->user_id;
        
        $data=array($header,$user_id[0]->user_id);
        
        if($cart==null){
            $cart=$this->checkcart($header);
        }

        if($product_id){
           if($cart){
              $value=array($cart,$product_id);
              //insert into cart
               if($this->cart_insert($value))
                    $this->response([
                    'status' => TRUE,
                    'cart_id'=>$cart,
                    'message' => 'product berhasil masuk cart'
                 ], REST_Controller::HTTP_OK); // OK (200) being the 
                else 
                     $this->response([
                    'status' => FALSE,
                    'message' => 'failed to generate cart'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code


           }else{
            //generate cart

            $cart=$this->generate_cart($data);
            if($cart){
               //insert into cart
                $value=array($cart,$product_id);
               if($this->cart_insert($value))
                    $this->response([
                    'status' => TRUE,
                    'cart_id'=>$cart,
                    'message' => 'product berhasil masuk cart'
                 ], REST_Controller::HTTP_OK); // OK (200) being the 
                else 
                     $this->response([
                    'status' => FALSE,
                    'message' => 'failed to generate cart'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }else{
                 $this->response([
                    'status' => FALSE,
                    'message' => 'failed to generate cart'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
            
           
           }
        }else{
            $this->response([
                    'status' => FALSE,
                    'message' => 'No product found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

    }

    public function deletefromcart_get()
    {
        $product_id=$this->get('product_id');
        $header=$this->input->get_request_header('X-API-KEY', TRUE);
        $cart=$this->get('cart_id');
        $user_id=$this->Booking_model->get_user_id_by_key($header);
       // echo $user_id[0]->user_id;
        
        $data=array($header,$user_id[0]->user_id);
        
        if($cart&&$product_id){
            $data=array('cart_id'=>$cart,'product_id'=>$product_id);
            $delete=$this->Booking_model->delete_single_product_from_cart($data);

            if($delete==TRUE){
                   $this->response([
                    'status' => TRUE,
                    'cart_id'=>$cart,
                    'message' => 'product berhasil dihapus dari cart'
                 ], REST_Controller::HTTP_OK); // OK (200) being the 

                 
               


           } else {
                     $this->response([
                    'status' => FALSE,
                    'message' => 'product gagal dihapus dari cart'
                 ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
           }
            
        }

      

    }
    public function cartupdate_post(){
        $products=$this->post('product_id');
             $cart=$this->post('cart_id');
             $qty=$this->post('qty');
             $note=$this->post('note');

        if($cart&&$products&&$qty){
                $data['product_id']=$products;
                $data['cart_id']=$cart;
                $value['qty']=$qty;
                $value['note']=$note;

                $status=$this->Booking_model->update_cart($data,$value);
                
            

            if($status){
                     $this->response([
                    'status' => TRUE,
                    'cart_id'=>$cart,
                    'message' => 'cart update sukses'
                 ], REST_Controller::HTTP_OK); // OK (200) being the 
            }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'failed to update cart'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }else{
            $this->response([
                    'status' => FALSE,
                    'message' => 'failed to update cart, no parameter found'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }



    }    

    public function cartcheckout_post(){
       

        $cart=$this->post('cart_id');
        $user_id=$this->post('user_id');
        $start=$this->post('start');
        $finish=$this->post('finish');
        $telp=$this->post('no_telp');
        $store=$this->post('store_id');

        if($cart&&$user_id&&$start&&$finish&&$store&&$telp){
                $booking=$this->bookprocess($cart,$user_id,$start,$finish,$store,$telp);
                if($booking!=0){
                    $this->response([
                                    'status' => TRUE,
                                    'book_id'=>$booking,
                                    'message' => 'proses booking berhasil dibuat'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
                }else{
                     $this->response([
                        'status' => FALSE,
                        'message' => 'Booking Gagal, hubungi admin'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
        }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'parameter not valid'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }



    }




    public function checkcart($key){
        $cart=$this->Booking_model->get_cart($key);
       
        if($cart){
            return $cart[0]->cart_id;
        }else{
                return null;
        }
        
    }
    public function generate_cart($data){
        $cart=$this->Booking_model->create_cart($data);
        return $cart;
        
    } 
    public function cart_insert($value){
        $cart=$this->Booking_model->insert_cart($value);
        return $cart;
        
    }

    /*booking process*/
    public function newbook_post(){
        $cart=$this->post('cart_id');
        $user_id=$this->post('user_id');
        $start=$this->post('start');
        $finish=$this->post('finish');
        $store=$this->post('store_id');
        
        
        if($cart&&$user_id&&$start&&$finish&&$store){
                $booking=$this->bookprocess($cart,$user_id,$start,$finish,$store);
                if($booking!=0){
                    $this->response([
                                    'status' => TRUE,
                                    'book_id'=>$booking,
                                    'message' => 'proses booking berhasil dibuat'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
                }else{
                     $this->response([
                        'status' => FALSE,
                        'message' => 'Booking Gagal, hubungi admin'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
        }else{
                $this->response([
                    'status' => FALSE,
                    'message' => 'parameter not valid'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function bookprocess($cart,$user,$start,$finish,$store,$telp){
        $data=array($cart,$user,$start,$finish,$store,$telp);
        //generate booking
        $booking=false;
        $booking=$this->Booking_model->create_book($data);

        return $booking;

    }
    /*category API call*/
    public function booklist_get()
    {
        $id=$this->get('user_id');
        if($id){
            $book_list=$this->Booking_model->customer_book_list($id);
             if(count($book_list)>0){
              $this->response([
                                    'status' => TRUE,
                                    'book_list'=>$book_list,
                                    'message' => 'sukses mendapatkan list'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
            }else{
                 $this->response([
                                    'status' => TRUE,
                                    'book_list'=>$book_list,
                                    'message' => 'tidak ada list'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
            }
        }else{
             $this->response([
                    'status' => FALSE,
                    'message' => 'parameter not valid'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

    } 
    public function storebooklist_get()
    {
        $id=$this->get('store_id');
        if($id){
            $book_list=$this->Booking_model->store_book_list($id);
             if(count($book_list)>0){
              $this->response([
                                    'status' => TRUE,
                                    'book_list'=>$book_list,
                                    'message' => 'sukses mendapatkan list'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
            }else{
                 $this->response([
                                    'status' => TRUE,
                                    'book_list'=>$book_list,
                                    'message' => 'tidak ada list'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
            }
        }else{
             $this->response([
                    'status' => FALSE,
                    'message' => 'parameter not valid'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

    }
    public function bookdetail_get()
    {
        $id=$this->get('book_id');
         if($id){
            $book_list=$this->Booking_model->customer_book_detail($id);
            if(count($book_list)>0){
              $this->response([
                                    'status' => TRUE,
                                    'book_list'=>$book_list,
                                    'message' => 'sukses mendapatkan list'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
            }else{
                 $this->response([
                                    'status' => TRUE,
                                    'book_list'=>$book_list,
                                    'message' => 'tidak ada list'
                                    ], REST_Controller::HTTP_OK); // OK (200) being the 
            }
        }else{
             $this->response([
                    'status' => FALSE,
                    'message' => 'parameter not valid'
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

    }

	
	

    public function booking_post()
    {
        
    }

     public function bookstatus_get()
    {
        
    }

    public function bookcancel_get()
    {
        
    }

    public function endbook_post()
    {
        
    }

}
