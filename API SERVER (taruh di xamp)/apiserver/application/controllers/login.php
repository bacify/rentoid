<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Login extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('user_model');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    
    }

     protected $methods = [
            'index_put' => ['level' => 10, 'limit' => 10],
            'index_delete' => ['level' => 10],
            'level_post' => ['level' => 10],
            'regenerate_post' => ['level' => 10],
        ];

    public function guest_get()
    {
        // Build a new key
        $key = $this->_generate_key();

        // If no key level provided, provide a generic key
        $level = $this->put('level') ? $this->put('level') : 1;
        $ignore_limits = ctype_digit($this->put('ignore_limits')) ? (int) $this->put('ignore_limits') : 1;
  		$ip_address=$_SERVER['REMOTE_ADDR'];
        // Insert the new key
        if ($this->_insert_key($key, ['level' => $level, 'ignore_limits' => $ignore_limits,'ip_addresses'=>$ip_address]))
        {
            $this->response([
                'status' => TRUE,
                'key' => $key
            ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'Could not save the key'
            ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // INTERNAL_SERVER_ERROR (500) being the HTTP response code
        }
    }

     public function auth_post()
    {
        
        $email=$this->post('email');
        $password=$this->post('password');
        $data=array('email'=>$email,
        			'password'=>$password);

        if($this->user_model->checklogin($data)){
        	$id=$this->user_model->get_id($data['email']);
            $user_data=$this->user_model->get_userdata($id);
        	$store_data=$this->user_model->get_storedata($id);
            $store_id='';
            foreach($store_data as $s){
                $store_id=$s->store_id;
            }

        	// Build a new key
			        $key = $this->_generate_key();
			        // If no key level provided, provide a generic key
			        $level = $this->put('level') ? $this->put('level') : 1;
			        $ignore_limits = ctype_digit($this->put('ignore_limits')) ? (int) $this->put('ignore_limits') : 1;
			        $ip_address=$_SERVER['REMOTE_ADDR'];

			        // Insert the new key
			        if ($this->_insert_key($key, ['level' => $level, 'ignore_limits' => $ignore_limits,'ip_addresses'=>$ip_address,'user_id'=>$id]))
			        {
			            $this->response([
			                'status' => TRUE,
			                'key' => $key,
                            'user_id'=>$id,
                            'store_id'=>$store_id
			            ], REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
			        }
			        else
			        {
			            $this->response([
			                'status' => FALSE,
			                'message' => 'Could not save the key'
			            ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // INTERNAL_SERVER_ERROR (500) being the HTTP response code
			        }
		}
		else{
			$this->response([
			                'status' => FALSE,
			                'message' => 'Wrong login detail'
			            ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR); // INTERNAL_SERVER_ERROR (500) being the HTTP response code
		}
     }  
     public function register_post()
    {
    	
    	$message = ['nama' => $this->post('nama'),
            'email' => $this->post('email'),
            'password' => $this->post('password'),
            'message' => '',
            'status' => '0'
        ];
    		 if(!$this->user_model->check_email($message['email'])&&$message['nama']!=''&&$message['email']!=''&&$message['password']!=''){
                $data=array(
                    'nama'=>$message['nama'],
                    'email'=>$message['email'],
                    'password'=>$message['password']
                );
                if($this->user_model->insert_user($data)){
                    $id=$this->user_model->get_id($data['email']);
                    //insert toko with id
                    if($this->user_model->insert_toko($id)){
                     //   echo 'insert toko sukses';
                    }else{
                      //  echo 'insert toko gagal';
                    }
                    $message['message'] ='User berhasil ditambahkan';
                    $message['status'] = 1;   
                }else{
                    $message['message'] ='User Gagal Ditambah';
                    $message['status'] = 0;     
                }
                
                 
                
           
            }else{
                 $message['message'] ='Pendaftaran gagal, email sudah didaftarkan';
                 $message['status'] = 0;
                
            }    
        	 $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

        



    /* Helper Methods */

    private function _generate_key()
    {
        do
        {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

            // If an error occurred, then fall back to the previous method
            if ($salt === FALSE)
            {
                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, config_item('rest_key_length'));
        }
        while ($this->_key_exists($new_key));

        return $new_key;
    }

    /* Private Data Methods */

    private function _get_key($key)
    {
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->get(config_item('rest_keys_table'))
            ->row();
    }

    private function _key_exists($key)
    {
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    private function _insert_key($key, $data)
    {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();

        return $this->rest->db
            ->set($data)
            ->insert(config_item('rest_keys_table'));
    }

    private function _update_key($key, $data)
    {
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->update(config_item('rest_keys_table'), $data);
    }

    private function _delete_key($key)
    {
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->delete(config_item('rest_keys_table'));
    }
}