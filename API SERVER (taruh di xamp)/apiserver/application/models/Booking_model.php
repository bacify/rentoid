<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Booking_model extends CI_Model {

       
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_cart($key)
        {       $this->db->where('api_key',$key);
                $this->db->limit(1);
                $query = $this->db->get('cart');
                return $query->result();
        }  
       
        public function get_cart_status_id($id)
        {       $this->db->where('cart_id',$id);
                $query = $this->db->get('cart');
                return $query->result();
        }

        public function create_cart($data)
        {       
               
                $key=$data[0];
                if($data[1]==null){
                    $user_id=0;
                }else{
                    $user_id=$data[1];
                }
                $value=array('user_id'=>$user_id,
                                 'api_key'=>$key);
    

                //$this->db->where('api_key',$key);
                
                $query = $this->db->insert('cart',$value);
                $id=null;
                if($query){
                    $id=$this->db->insert_id();
                }
                return $id;
        }

          public function insert_cart($data)
        {       
               
                $value=array('cart_id'=>$data[0],
                                 'product_id'=>$data[1]);
                
                $qty=$this->get_cart_qty($value)+1;
               $value2['qty']=$qty;
                
                //$this->db->where('api_key',$key);
                if($qty==1){
                    $value['qty']=$qty;
                    $query = $this->db->insert('cart_detail',$value);
                }
                else{
                    $this->db->where($value);
                    $query = $this->db->update('cart_detail',$value2);
                }

                if($query)
                    return true;
                else 
                    return false;
        }
         public function update_cart($data,$value)
        {       
               
                
                $this->db->where($data);
                 $query = $this->db->update('cart_detail',$value);

                if($query)
                    return true;
                else 
                    return false;
        }

          public function get_cart_qty($data)
        {       
               
                $this->db->where($data);
                $query = $this->db->get('cart_detail');
                $result=$query->result();
                if($result)
                    {
                        return $result[0]->qty;
                    }
                else 
                    return 0;
        }
                
            public function create_book($data)
        {       
               
                $value=array('user_id'=>$data[1],  
                             'start_date'=>$data[2],
                             'finish_date'=>$data[3],
                             'store_id'=>$data[4],
                             'telp'=>$data[5],
                             'status'=>'1');
                $product_cart=$this->get_product_cart($data[0]);
                 //  print_r($value);
                //  print_r($product_cart);
                //Generate order
                if(count($product_cart)>0)
                    $generate=$this->generate_order($value);
                else
                    $generate=0;

                if($generate!=0){
                    //insert product new order
                    $total=0;
                    foreach($product_cart as $product){
                        if($product->store_id==$value['store_id']){
                            $total=$total+$product->price;
                            $value2=array('book_id'=>$generate,
                                         'product_id'=>$product->product_id,
                                         'qty'=>$product->qty,
                                         'note'=>$product->note);
                            $this->insert_product_to_order($value2);
                            $value3=array('cart_id'=>$data[0],
                                          'product_id'=>$product->product_id);
                            $this->delete_product_from_cart($value3);
                        }
                    }
                    $this->update_book_price($generate,$total);
                }
                return $generate;
        }
        public function update_book_price($book_id,$price){
            $data=array('total'=>$price);
            $this->db->where('book_id',$book_id);
            $this->db->update('book',$data);

        }
        public function generate_order($data){
            $r=$this->db->insert('book',$data);
            if($r)
            {
                return  $this->db->insert_id();
            }
            else{
                return 0;
            }
        }   
        public function insert_product_to_order($data){
            $r=$this->db->insert('book_detail',$data);
            return $r;
        } 
        public function delete_product_from_cart($data){
            
            $this->db->delete('cart_detail',$data);
            $cart_status=$this->check_cart_status($data['cart_id']);
            if(!$cart_status){
                $this->delete_cart_by_id($data['cart_id']);
            }
             if ( $this->db->affected_rows() == '1' ) {
                return TRUE;
            }
           else {return FALSE;}
        }
        public function delete_single_product_from_cart($data){
          //print_r($data);
            
            $this->db->delete('cart_detail',$data);
         if ( $this->db->affected_rows() == '1' ) {
                return TRUE;
            }
           else {
            return FALSE;
            }
           
        }
        public function check_cart_status($cart_id){
            $data=$this->get_cart_by_id($cart_id);
            if(count($data)>0){
                return true;
            }
            else{
                return false;
            }
        } 
       
        public function delete_cart_by_id($cart_id){
            $data=array('cart_id'=>$cart_id);
            
            $this->db->delete('cart',$data);
            if ( $this->db->affected_rows() == '1' ) {
                return TRUE;
            }
           else {return FALSE;}
        }
        public function get_cart_by_id($cart_id){
            $this->db->where('cart_id',$cart_id);
            $r=$this->db->get('cart_detail');
            return $r->result();
        }
        public function get_product_cart($cart){
            $this->db->select('product.*,cart_detail.*,store.name as store_name');
            $this->db->where('cart_id',$cart);
            $this->db->join('product','cart_detail.product_id=product.product_id');
            $this->db->join('store','store.store_id=product.product_id');

            $this->db->order_by('product.store_id');
            $query=$this->db->get('cart_detail');

            return $query->result();
        } 
        public function customer_book_list($id){
            $this->db->where('user_id',$id);
            $this->db->order_by('book_date');
            $query=$this->db->get('book');

            return $query->result();
        }

        public function customer_book_detail($id){
            $this->db->where('book_id',$id);
            $this->db->join('product','product.product_id=book_detail.product_id');
            $query=$this->db->get('book_detail');

            return $query->result();
        }

        public function store_book_list($id){
            $this->db->where('store_id',$id);
            $this->db->order_by('book_date');
            $query=$this->db->get('book');

            return $query->result();
        }
         public function get_user_id_by_key($key){
            $this->db->where('key',$key);
            $this->db->limit('1');
            $this->db->order_by('id','desc');
            $query=$this->db->get('api_keys');
            return $query->result();
        }
                

}