<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User_model extends CI_Model {

       
        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_users()
        {
                $query = $this->db->get('users');
                return $query->result();
        }
        public function check_email($email)
        {       
                $this->db->select('count(*) as x');
                $this->db->where('email',$email);
                $query = $this->db->get('user');
                $result;
                foreach($query->result() as $row){
                    $result=$row->x;
                }
                if($result=='0'){
                    return false;
                }
                else{
                    return true;
                }
        }
        

        public function insert_user($data){

            if($this->db->insert('user',$data)){
                return true;
            }else {
                return false;
            }               
        }

        public function update_user($data,$email){
            $this->db->where('email',$email);
            if($this->db->update('user',$data)){
                return true;
            }else {
                return false;
            }               
        }
        public function checklogin($data){
            $this->db->select('count(*) as x');
            $this->db->where($data);
            $query = $this->db->get('user');
            $result;
                foreach($query->result() as $row){
                    $result=$row->x;
                }
                if($result=='0'){
                    return false;
                }
                else{
                    return true;
                }           
        }

        public function get_id($data){
            $this->db->select('user_id');
            $this->db->where('email',$data);
             $query = $this->db->get('user');
            $result;
                foreach($query->result() as $row){
                    $result=$row->user_id;
                }
            return $result;
                
        }
        public function insert_toko($data){
            $r=$this->get_userdata($data);
            $d=array('user_id'=>$data,
                    'name'=>'',
                    'address'=>'');
            foreach($r as $b){
                $d['name']=$b->nama;

            }
             if($this->db->insert('store',$d)){
                return true;
            }else {
                return false;
            }    
                
        }
    public function get_userdata($id){
            $this->db->where('user_id',$id);
            $query = $this->db->get('user');
               
            return $query->result();
                
        }

        public function get_storedata($id){
            $this->db->where('user_id',$id);
            $query = $this->db->get('store');
               
            return $query->result();
                
        }
         public function update_store($data,$id){
            $this->db->where('store_id',$id);
            if($this->db->update('store',$data)){
                return true;
            }else {
                return false;
            }               
        }
          public function insert_product($data){
            
             if($this->db->insert('product',$data)){
                return true;
            }else {
                return false;
            }    
                
        }
         public function get_products()
        {
                $query = $this->db->get('product');
                return $query->result();
        }
        public function get_product_by_id($id)
        {       
                $this->db->select('product_id,product.store_id,product.name as name,price,product_desc,image,user_id,store.name as storename,address');
                $this->db->where('product_id',$id);
                $this->db->join('store','store.store_id=product.store_id');
                $query = $this->db->get('product');
                return $query->result();
        }
        public function get_product_by_filter($filter)
        {       $this->db->where($filter);
                $query = $this->db->get('product');
                return $query->result();
        }

}